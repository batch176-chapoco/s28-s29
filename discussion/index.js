//[OBJECTIVE] Create a server-side app using Express Web Framework.

//Related this task to something that you do on a daily basis.

//[SECTION] Append the entire app to our node package manager.

//package.json -> the "heart" of every node project. this also contains different metadata that describes the structure of the project.

//scripts -> is used to declare and describe custom commands and keywords that can be used to execute a project with the correct runtime environment.

//NOTE: "start" is globally recognized amongst node projects and frameworks as the 'default' comman script to execute a task/project
//however for *unconventional* keywords or command you have to append the command "run"
//SYNTAX: npm run <custom command>

//[COOKING]
//1. Identify and prepare the ingredients.
const express = require("express");

//express => will be used as the main component to create the server.
//we need to be able to gather/acquire the utilities and components needed that the express library will provide us.
//=> require() -> directive used to get the library/component needed inside the module.

//prepare the environment in which the project will be served.

//[SECTION] Preparing a remote repository for our node Project.

//NOTE: always DISABLE the node_modules folder
//WHY? -> it will take too much space in our repository and making it alot more difficult to stage upon commiting the changes in our remote repo.
// -> if ever that you will deploy your node project on deployment platforms like (heroku,netlify,vercel) the project will automatically be rejected because node_modules on various deployment platforms.

//HOW? using a .gitignore module


//[SECTION] Create a Runtime environment that automatically autofits all the changes in our app.
//you can even insert items like text art into your run time environment.

console.log(`Welcome to our Express API Server ( ͡❛ ͜ʖ ͡❛)`);

